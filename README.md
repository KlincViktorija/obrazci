<table align="center"><tr><td align="center" width="9999">
<img src="./src/images/logo.png" align="center" width="150" alt="Project logo">

# Obrazci

Spletna platforma za ustvarjanje in dinamično reševanje obrazcev
</td></tr></table>

# O obrazcih

DocGen je platforma za ustvarjanje in generiranje rešljivih obrazcev. Platforma je sestavljena iz dveh delov: ustvarjanje obrazca in reševanje obrazca. Z implementirano funkcijo drag and drop je uporabniško zelo prijazna, kreativna in dinamična rešitev, z razvitim prikazom rešenega obrazca, kar v aplikaciji.

Ključna prednost spletne aplikacije je njena možnost dinamičnosti in nadaljnje nadgradnje. 

## Ustvarjanje obrazca

Funkcionalnost ustvarjanja obrazca je podprta z dvema večjima knjižnicama [CKEditor 4](https://ckeditor.com/ckeditor-4/), ki nam omogoča bogato urejanje besedila ter [smooth dnd](https://github.com/kutlugsahin/react-smooth-dnd/), katera omogoča premikanje elementov med besedilnim editorjem ter seznamom možnih komponent za uporabo.
Obogaten tekstovni urejevalnik dovoljuje poljubno oblikovanje in urejanje obrazca. S pomočjo "drag & drop" komponent, ki jih umestimo v besedilo, lahko določimo, katere informacije želimo, da oseba, ki rešuje obrazec vnese. Te komponente so tudi prilagodljive uporabnikom, tako da lahko ustreza vsem potrebam. Obrazci se s klikom na gumb shranijo v inovativno bazo MongoDB.

<img src="./_PROMOCIJA/Screenshoti/create_1.png">

## Reševanje obrazca

Uporabniku se prikaze iz [MongoDB](https://www.mongodb.com/) pridobljen seznam obrazcev. Po izbiri enega izmed teh se uporabniku prikaze dinamičen seznam vprašanj katere je postavil avtor obrazca, ta so povezana s kreiranimi inputi. Odgovori na vprašanja zamenjajo prej postavljene inpute, ter izpišejo na spletni strani v obliki dokumenta.

<img src="./_PROMOCIJA/Screenshoti/wizard.png">

# Predpogoji
```
Visual Studio Code v. 1.56,
Node.js 12.19.0,
Docker Desktop 3.4.0 (za docker zagon)
```

# Navodila za zagon aplikacije - docker

1. Prenesite zip datoteko docker različice te aplikacije [tukaj](https://univerzamb-my.sharepoint.com/:u:/r/personal/sasa_jovanovic_student_um_si/Documents/obrazci_docker/obrazci_docker.zip?csf=1&web=1&e=epFZYu), najdete pa tudi tukaj v mapi docker-zip. 

2. Datoteko odprite v Visual Studio Code

3. V terminal vnesite:
```
$ docker-compose up -d
```
4. Ko se proces konča pojdite v Docker Desktop, v zavihek Images in zaženite obrazci-docker-server in obrazci-docker-client.

5. V brskalniku je sedaj aplikacija na voljo na naslovu: http://localhost:3000.

# Navodila za zagon aplikacije - npm 

1. Klonirajte ta repozitorij na vaš računalnik ali pa prenesite zip [tukaj](https://univerzamb-my.sharepoint.com/:u:/r/personal/sasa_jovanovic_student_um_si/Documents/obrazci_npm/obrazci.zip?csf=1&web=1&e=P5yU7G), najdete pa tudi tukaj v mapi docker-zip.

3. Odprite kodo v Visual Studio Code.

2. V terminal vnesite:

```
$ npm install
```
3. Aplikacijo zaženite z vpisom v terminal:
```
$ npm start
```
4. V brskalniku na voljo na naslovu: http://localhost:3000.

# Avtorice
<table align="center">
    <tr>
        <td align="center" width="9999">
        [Viktorija Klinc](https://gitlab.com/KlincViktorija)
        </td>
        <td align="center" width="9999">
        [Saša Jovanović](https://gitlab.com/sasa.jovanovic5)
        </td>
        <td align="center" width="9999">
        [Katja Denžič](https://gitlab.com/Kat3434)
        </td>
    </tr>
</table>
