import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import TextEditor from './Components/TextEditor';
import Home from './Components/Home';
import List from './Components/List';
import Footer from './Components/Footer';
import Navbar from './Components/Nav';
import Wizard from './Components/Wizard';

export default function App() {
    return (
        <div className="App">
            <div> 
                <Router>
                    <Navbar/>
                    <Route exact path="/">
                        <Home></Home>
                    </Route>
                    <Route exact path="/create" >
                        <TextEditor></TextEditor>
                    </Route>
                    <Route path="/list" exact component={List}>
                        <List></List>
                    </Route>
                    <Route path="/wizard/:id" component={Wizard}>
                    </Route>
                    <Footer />
                </Router>
            </div>
        </div>
    );
}
