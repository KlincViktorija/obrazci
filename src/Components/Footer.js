import React from 'react';
import Container from '@material-ui/core/Container';
import { withStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';

const useStyles = theme => ({
    
    root: {
        position: 'relative',
        bottom: 0,
        width: '100%',
    }

});

class Footer extends React.Component{
    
    constructor (props) {
        super(props);
    }

    render(){

        const { classes } = this.props;
        
        return(
            <footer>
                <div className={classes.root}>
                <Box px={{xs: 3, sm: 1}} py={{xs: 5, sm: 3}} bgcolor="white" border="#d3d3d3" color="#00008b" height="80px">
                    <Container maxWidth="lg" height="10px">
                        <Box textAlign="center" pt={{xs: 5, sm: 0}} pb={{xs: 5, sm: 5}}>
                            Obrazci &reg; {new Date().getFullYear()}
                        </Box>
                    </Container>
                </Box>
                </div>
            </footer>
        );
    }
}
export default withStyles(useStyles)(Footer)