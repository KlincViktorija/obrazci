import React, { Component } from 'react';
import axios from 'axios';
import { Container, Draggable } from 'react-smooth-dnd';
import Popup from './PopUp';
import tekst from '../images/tekst.png';
import ime from '../images/ime.png';
import priimek from '../images/priimek.png';
import emso from '../images/emso.png';
import davcna from '../images/davcna.png';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

const useStyles = theme => ({
    btnSubmitDnd: {
        top: '15px',
        backgroundColor: '#fffbf7',
    },
    instruciotns: {
        top: '145px',
    }
});

let duplicate = 1;

const applyDrag = (arr, dragResult) => {
  const { removedIndex, addedIndex, payload } = dragResult;
  if (removedIndex === null && addedIndex === null) return arr;

  const result = [...arr];
  let itemToAdd = payload;

  if (removedIndex !== null) {
    itemToAdd = result.splice(removedIndex, 1)[0];
  }

  if (addedIndex !== null) {
    result.splice(addedIndex, 0, itemToAdd);
  }

  duplicate = duplicate + 1;

  return result;
};

const getIMG = (img) => {
    if (img === 'Text') return tekst
    else if (img ==='Name') return ime
    else if (img === 'Surname') return priimek
    else if (img === 'EMSO') return emso
    else if (img === 'TIN') return davcna
}

class Dnd extends Component {
    
    componentDidMount() {
        axios.get('http://localhost:8082/elementi/')
          .then(response => {
              console.log(JSON.stringify(response))
              const items1 = response.data;
            this.setState({items1});
          })
          .catch((error) => {
            console.log(error);
          })
    }

    constructor() {
        super();

        this.state = {
        items1: [],
        items2: [],
        popup: false,
        currentPopUp: '',
        type: ''
        }

        this.submit = this.submit.bind(this);
        this.togglePopup = this.togglePopup.bind(this);
    }

    togglePopup = (e, element, id) => {
        e.preventDefault();
        
        this.setState({
          showPopup: !this.state.showPopup,
          currentPopUp: id,
          type: element
        });
    }

    submit = (e) => {
        e.preventDefault();

        this.setState({
            items1: this.state.items1,
            items2: []
        });
    }

    handleSubmit = (temp) => {
        this.props.onsubmit(temp);
    }

    render() {

        const { classes } = this.props;

        return (
            <>
                <Box textAlign="center" direction="row">
                    <Grid container spacing={0}>
                        <Grid item xs={6} direction="row">
                            <h3 >AVAILABLE</h3>
                            <Container groupName="1" behaviour="copy" getChildPayload={i => this.state.items1[i]} onDrop={e => this.setState({ items1: applyDrag(this.state.items1, e) })}>
                            
                                {
                                this.state.items1.map((p,i) => {
                                    return (
                                    <Draggable key={i}>
                                        <div className="draggable-item">
                                        <img src={getIMG(p.vrsta)} alt={p.vrsta+this.counte} height="20" name={p.vrsta}/>
                                        </div>
                                    </Draggable>
                                    );
                                })
                                }
                            </Container>
                        </Grid>
                        <Grid item xs={6} direction="row">
                            <h3 >IN USE</h3>
                            <Container groupName="1" getChildPayload={i => this.state.items2[i]} onDrop={e => this.setState({ items2: applyDrag(this.state.items2, e) })}>
                            
                                {
                                this.state.items2.map((p) => {
                                    return (
                                        <>
                                        <img src={getIMG(p.vrsta)} alt={p.vrsta+duplicate} height="20" name={p.vrsta} onDoubleClick={((x) => this.togglePopup(x, p.vrsta, p.vrsta+duplicate))}/>
                                        {this.state.showPopup ? 
                                            <Popup
                                                text={p.vrsta}
                                                element={p.vrsta}
                                                id={p.vrsta+duplicate}
                                                closePopup={this.togglePopup.bind(this)}
                                                onsubmit={this.handleSubmit.bind(this)}
                                            />
                                            : null
                                        }   
                                        </>
                                    );
                                })
                                }
                            </Container>
                        </Grid> 
                    </Grid>
                    <Button variant="outlined" color="primary" onClick={this.submit.bind(this)} className={classes.btnSubmitDnd}>
                        SUBMIT 
                    </Button>
                </Box>

                <Box textAlign="center" p={10} className={classes.instruciotns}>
                    <Typography variant="caption" gutterBottom> 
                        Drag into the IN USE field.<br/>
                        Double-click to set its settings.<br/>
                        Drag into the text editor.<br/>
                        Reuse: copy and paste img<br/>
                        Start new input: submit
                    </Typography>
                    
                </Box>
            </>
        );
    }
}
export default withStyles(useStyles)(Dnd);
