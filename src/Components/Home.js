import React from 'react';
import Container from 'react-bootstrap/Container';
import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import logo from '../images/logo.png';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Image from '../images/background.jpg';

const useStyles = theme => ({
    
    root: {
        padding: '10px',
        backgroundImage: `url(${Image})`,
        backgroundSize: 'cover',
        height: '90vh',
    },
    paper: {
        padding: '20px',
        marginTop: '25%',
        minWidth: 200,
        justifyContent: 'center',
        textAlign: 'center'
    },
    container: {
        padding: '10px',
    },
    media: {
        marginBottom: 12
    },
    btn: {
        justifyContent: 'center'
    }
});


class Home extends React.Component{

    constructor (props) {
        super(props);
    }

    render(){

        const { classes } = this.props;

        return(
            <div className={classes.root}>
                <React.Fragment>
                <CssBaseline />
                    <Container maxWidth="sm" className={classes.container}>
                        <div className={classes.paper}>
                            <CardContent>
                                <img src={logo} className={classes.media}></img>
                                <Typography gutterBottom variant="h5" component="h2">
                                    Create interactive online forms for easy use.
                                </Typography>
                            </CardContent>
                            <CardActions className={classes.btn}>
                                <Button href="/create" variant="outlined" color="primary">
                                    Create
                                </Button>
                            </CardActions>
                        </div>
                    </Container>
                </React.Fragment>
            </div>
        );
    }
}

export default withStyles(useStyles)(Home)