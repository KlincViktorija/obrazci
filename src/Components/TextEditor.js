import React from 'react';
import axios from 'axios';
import Form from 'react-bootstrap/Form';
import CKEditor from 'ckeditor4-react';
import Dnd from './Dnd';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import styles from '../CssModules.module.css';

const useStyles = theme => ({
    btnSubmit: {
        top: '60px',
    },
});

class TextEditor extends React.Component{

    constructor( props ) {
        super( props );

        this.state = {
            naslov: '',
            datoteka: '',
            elementi: []
        };

        this.onChange = this.onChange.bind( this );
        this.onEditorChange = this.onEditorChange.bind( this );
        this.onSubmit = this.onSubmit.bind( this );
        this.handleSubmit = this.handleSubmit.bind( this );
    }

    onEditorChange = (event) => {
        this.setState( {
            datoteka: event.editor.getData()
        } );
    }

    onChange = (event) => {
        const target = event.target;
        const {name, value} = target;

        this.setState({
            [name]:value
        });
    }

    onSubmit = (event) => {
        event.preventDefault();

        const obrazecSchema = {
            naslov: this.state.naslov,
            datoteka: this.state.datoteka,
            elementi: this.state.elementi
        }
        axios.post('http://localhost:8082/obrazci/add', obrazecSchema)
            .then(res => console.log(res.data));

        window.location = '/list';
    }

    handleSubmit = (added) => {
        let temp = this.state.elementi;
        temp.push(added);
        
        this.setState({
            elementi: temp
        });
    }

    render(){

        const { classes } = this.props;

        return(
            <div className={styles.textEditorContainer}>
                <Box p={1}>
                    <Grid container spacing={0} className={classes.root}>
                        <Grid item xs={8} className={styles.textEditorBox}>
                            <Box textAlign="center">
                                <Form.Group>
                                    <Form.Control size="lg" type="text" name="naslov" value={this.state.naslov} onChange={this.onChange} placeholder="Document title" />
                                </Form.Group>
                                <CKEditor
                                    name="editor"
                                    data={this.state.datoteka}
                                    onChange={this.onEditorChange}
                                />
                                <Button variant="outlined" color="primary" onClick={this.onSubmit} title="when set simply close popup" className={classes.btnSubmit}>
                                    SUBMIT 
                                </Button>
                            </Box>
                        </Grid>
                        <Grid item xs={4} >
                            <Box p={3} >
                                <Dnd onsubmit={this.handleSubmit.bind(this)}/>
                            </Box>
                        </Grid>
                    </Grid>
                </Box>
            </div>
        );
    }
}
export default withStyles(useStyles)(TextEditor);