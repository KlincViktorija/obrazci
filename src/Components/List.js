import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import List1 from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import AssignmentRoundedIcon from '@material-ui/icons/AssignmentRounded';

const useStyles = theme => ({
    root: {
        padding: '10px',
        margin: '10px'
    },
    paper: {
        padding: '20px',
        margin: '10px',
        backgroundColor: '#edf2fb'
    },
    container: {
        padding: '10px',
        display: 'flex',
        minHeight: '78vh',
        flexDirection: 'column',
    }
});
  
class List extends React.Component{
 
    componentDidMount() {
        axios.get('http://localhost:8082/obrazci/')
         .then(response => {
            const obrazci = response.data;
          this.setState({obrazci});
         })
         .catch((error) => {
            console.log(error);
         });
    }

    constructor(props) {
        super(props);
        this.state = {obrazci: []};
    }

    render(){
        const { classes } = this.props;
        return(
            <div className={classes.root}>
            <React.Fragment>
            <CssBaseline />
            <Container maxWidth="sm" className={classes.container}>
                <Paper className={classes.paper}>
                    <Typography variant="h5" component="h2">List of forms</Typography>
                        {
                        this.state.obrazci.map((v) => {
                            return ( 
                                <List1 component="nav" className={classes.root} aria-label="mailbox folders">
                                <ListItem button>
                                  <ListItemText primary={<><AssignmentRoundedIcon color="action"/><Link className="btn" to={'/wizard/'+v._id}>{v.naslov}</Link></>} />
                                </ListItem>
                                <Divider />
                              </List1>
                            );
                        })
                        }
                </Paper>
            </Container>
            </React.Fragment>
            </div>
        );
    }
}export default withStyles(useStyles)(List)