import React from 'react';
import axios from 'axios';
import Form from 'react-bootstrap/Form';
import { withStyles } from '@material-ui/core/styles';
import ReactDOM from 'react-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Card from '@material-ui/core/Card';
import Paper from '@material-ui/core/Paper';
import CardContent from '@material-ui/core/CardContent';
import Footer from './Footer';
import Navbar from './Nav';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import AssignmentRoundedIcon from '@material-ui/icons/AssignmentRounded';

const useStyles = theme => ({
    root: {
      padding: '10px',
      margin: '10px'
    },
    paper: {
        padding: '20px',
        margin: '10px',
        backgroundColor: '#a3b7e8'
    },
    container: {
        padding: '10px',
    },

    root2: {
        padding: '10px',
        margin: '10px',
        alignItems: "center",
        justify: "center"
      },
      bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
      },
      title: {
        fontSize: 14,
      },
      pos: {
        marginBottom: 12,
      },
});


class Wizard extends React.Component{
    
    componentDidMount() {
        let path = window.location.pathname;
        let ids = path.split("/");
        let id = ids[ids.length-1];
        axios.get('http://localhost:8082/obrazci/'+id)
         .then(response => {
            const obrazec = response.data;
          this.setState({obrazec});
         })
         .catch((error) => {
            console.log(error);
         })

    }

    constructor(props) {

        super(props);
        this.state = {
            obrazec: [], 
            odgovor: [],
        };
        
        this.onSubmit = this.onSubmit.bind( this );
        this.onChange = this.onChange.bind( this );
    }

    onChange = (index, event) => {
        var odg = this.state.odgovor.slice();
        odg[index] = event.target.value;
        this.setState({odgovor: odg})
    }

    
    onSubmit = (event) => {
        event.preventDefault();
        var glavniS = "";
        const odgovorSchema = { //shranjevanje id vprasanja in odgovor na to vprasanje
            odgovor: this.state.odgovor,
            id: this.state.id
        }

        const datoteka = this.state.obrazec.datoteka;
        
        var str = datoteka.split("<img");
        str.forEach(async function(el) {
            if(str.indexOf(el)===0){
                glavniS += el;
                
            }else{
                var odg = odgovorSchema.odgovor[str.indexOf(el)-1];
                var str1 = str[str.indexOf(el)].split(" />");
                glavniS += odg+ str1[1];
            }
        });

        function handleRefresh (e) {
            window.location.reload(false);
        }

        const { classes } = this.props;

        const App = () => (
            <Router>
            <Navbar/>
            <Container maxWidth="md" style={{paddingBottom: '300px'}}>
            <Card className={classes.root2} variant="outlined" >
            <CardContent className="editor-preview">
                <Typography className={classes.title} color="textSecondary" gutterBottom><AssignmentRoundedIcon color="action"/></Typography>
                <Typography variant="h5" component="h2" dangerouslySetInnerHTML={ { __html: this.state.obrazec.naslov } }></Typography>
                <Typography className={classes.pos} color="textSecondary"></Typography>
                <Typography variant="body2" component="p" dangerouslySetInnerHTML={ { __html: glavniS } }></Typography>
                <Button variant="contained" color="primary" onClick={handleRefresh}>BACK</Button>
            </CardContent>
            </Card>
            </Container>
            <Footer />
            </Router>
        );

        ReactDOM.render(
            <App />
            , document.getElementById('root'));
    }


    render() {
        const { classes } = this.props;
        return (
            <div className={classes.root}>
            <React.Fragment>
            <CssBaseline />
            <Container maxWidth="sm" className={classes.container}>
                <Paper className={classes.paper}>
                {
                (this.state.obrazec.elementi || []).map((v, index) => {
                    return (
                            <Card className={classes.root}>
                            <CardContent>
                                <Typography variant="h5" component="h2">{v.vprasanje}</Typography>
                                <Typography className={classes.pos} color="textSecondary"></Typography>
                                <Typography variant="body2" component="p">
                                <Form className={classes.root}>
                                <TextField id="standard-basic" size="lg" type="text" name="odgovor" key={index} value={this.state.odgovor[index]} onChange={this.onChange.bind(this, index)}  placeholder="Odgovor" />
                                <Form.Control type="hidden" name="id" value={this.state.id}/>
                                </Form>
                                </Typography>
                            </CardContent>
                            </Card>
                    );
                })
                }
                <Button variant="outlined" color="primary" name="submit" onClick={this.onSubmit} >Submit</Button>
                </Paper>
            </Container>
            </React.Fragment>
            </div>
        );
    }
}export default withStyles(useStyles)(Wizard);