import React from 'react';
import styles from '../CssModules.module.css';
import Form from 'react-bootstrap/Form';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';

const useStyles = theme => ({
    space: {
        margin: '7px',
    },
});

class Popup extends React.Component {

    onChange = (event) => {
        const target = event.target;
        const {name, value} = target;

        this.setState({
            [name]:value
        });
    }

    constructor(props) {
        super(props);

        this.state = {
        id: this.props.id,
        element: this.props.element,
        obvezno: true,
        vprasanje: '',
        stZnakov: ''
        }

        this.onChange = this.onChange.bind( this );
        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit = (event) => {
        event.preventDefault();
        this.props.onsubmit(this.state);
    }

    render() {

        const { classes } = this.props;

        return (
            <div className={styles.popup}>
                <div className={styles.inner}>
                    <h1>{this.props.text}</h1>
                    <Form.Group>
                        <div className={classes.space}>
                            <Form.Check size="lg" type="radio" name="obvezen" checked={this.state.obvezno} onClick={() => this.setState({obvezno: !this.state.obvezno})} label="Required" />
                        </div>
                        <div className={classes.space}>
                            <Form.Control required size="lg" type="text" name="vprasanje" value={this.state.vprasanje} onChange={this.onChange} placeholder="Question" />
                        </div>
                        <div className={classes.space}>
                            <Form.Control required size="lg" type="number" name="stZnakov" value={this.state.stZnakov} onChange={this.onChange} placeholder="Max. number of characters" />
                        </div>
                    </Form.Group>
                    <Button variant="outlined" color="primary" onClick={this.onSubmit} title="when set simply close popup">
                        SET 
                    </Button>
                    <div className={classes.space}/>
                    <Button variant="outlined" color="primary" onClick={this.props.closePopup}>
                        x
                    </Button>
                </div>
            </div>
        );
    }
}
export default withStyles(useStyles)(Popup);
