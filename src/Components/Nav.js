import React from 'react';
import logo from '../images/logo_beli_sm.png';
import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';

const useStyles = theme => ({
    
    linkDecor: {
        '&:hover': {
            color: '#d3d3d3'
        }
    },
    myToolbar: {
        justifyContent: 'space-between'
    },
    nav: {
        backgroundColor: "#4a4a4a",
    }

});

class Nav extends React.Component {

    constructor(props) {
        super(props);
    }

    render(){
        
        const { classes } = this.props;

        const links = [
            {name: "Home", path:"/"},
            {name: "Create", path:"/create"},
            {name: "List", path:"/list"},
        ];

        return(
            <div className={classes.root}>
                <React.Fragment>
                    <CssBaseline />
                    <AppBar position="static" className={classes.nav}>
                        <Toolbar className={classes.myToolbar}>
                            <IconButton href="/">
                                <img src={logo} height="45" className="d-inline-block align-top" alt="DOCGEN" />
                            </IconButton>
                            <ButtonGroup variant="text" color="primary">
                                {links.map((link,index) => (
                                    <>
                                        <Button key={index} href={link.path} className={classes.linkDecor} color="inherit">
                                            {link.name}
                                        </Button>
                                    </>
                                ))}
                            </ButtonGroup>
                        </Toolbar>
                    </AppBar>
                </React.Fragment>
            </div>
        );
    }
}
export default withStyles(useStyles)(Nav)