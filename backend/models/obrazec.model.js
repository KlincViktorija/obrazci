const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const obrazecSchema = new Schema({
    
    naslov: {
        type: String,
        required: true
    },
    datoteka: {
        type: String,
        required: true
    },
    elementi: []
});

const Obrazec = mongoose.model('Obrazec', obrazecSchema);

module.exports = Obrazec;