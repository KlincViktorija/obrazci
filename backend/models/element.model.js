const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const elementSchema = new Schema({
    vrsta: {
        type: String,
        required: true,
    },
    img: {
        type: String,
        required: true,
    },
    obvezno: {
        type: Boolean
    },
    vprasanje:{
        type: String
    },
    stZnakov: {
        type: Number
    }
});


const Element = mongoose.model('Element', elementSchema);

module.exports = Element;