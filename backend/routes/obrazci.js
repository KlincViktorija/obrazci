const router = require('express').Router();
let Obrazec = require('../models/obrazec.model');


router.route('/').get((req, res) => {
  Obrazec.find().then(obrazci => res.json(obrazci)).catch(err => res.status(400).json('Error: ' + err));
});

router.route('/add').post((req, res) => {
  
  const newObrazec = new Obrazec({
    naslov: req.body.naslov,
    datoteka: req.body.datoteka,
    elementi: req.body.elementi
  });

  newObrazec.save().then(() => res.json('Obrazec added!')).catch(err => res.status(400).json('Error: ' + err));
});


router.route('/:id').get((req, res) => {
  Obrazec.findById(req.params.id).then(obrazec => res.json(obrazec)).catch(err => res.status(400).json('Error: ' + err));
});

router.route('/:id').delete((req, res) => {
  Obrazec.findByIdAndDelete(req.params.id).then(() => res.json('Obrazec deleted.')).catch(err => res.status(400).json('Error: ' + err));
});

module.exports = router;