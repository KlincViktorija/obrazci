const router = require('express').Router();
let Element = require('../models/element.model');

router.route('/').get((req, res) => {
    Element.find().then(elementi => res.json(elementi)).catch(err => res.status(400).json('Error: ' + err));
});

router.route('/dodaj').post((req, res) => {
    const vrsta = req.body.vrsta;
    const newVrsta = new Element({vrsta});
    newVrsta.save().then(() => res.json('Element added!')).catch(err => res.status(400).json('Error: ' + err));
});


router.route('/:id').get((req, res) => {
    Element.findById(req.params.id).then(element => res.json(element)).catch(err => res.status(400).json('Error: ' + err));
});


router.route('/:vrsta').get((req, res) => {
    let parameter = req.params.vrsta;
    
    
    Element.find({vrsta: parameter}).then(element => res.json(element)).catch(err => res.status(400).json('Error: ' + err));
});

router.route('/:id').delete((req, res) => {
    Element.findByIdAndDelete(req.params.id).then(() => res.json('Element deleted.')).catch(err => res.status(400).json('Error: ' + err));
});

module.exports = router;