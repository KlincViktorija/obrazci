const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

require('dotenv').config();

const app = express();
const port = process.env.port || 8082;

app.use(cors());
app.use(express.json());

const uri = process.env.ATLAS_URI;
mongoose.connect(uri, {useNewUrlParser: true, useCreateIndex: true});

const connection = mongoose.connection;

connection.once('open', () => {
    console.log("MongoDB database connection established successfully");
})

const obrazciRouter = require('./routes/obrazci');
const elementiRouter = require('./routes/elementi');

app.use('/obrazci', obrazciRouter);
app.use('/elementi',elementiRouter);

app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});

/*
const connectDB = require('./config/db');

const obrazci = require('./routes/api/obrazci');

connectDB();

app.use(cors({origin: true, credentials: true}));

app.use(express.json({ extended: false }));

app.get('/', (req, res) => res.send('Hello world!'));

app.use('routes/api/obrazci', obrazci);


*/